﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    private GameManager gm;

    private void Start()
    {
        gm = GetComponent<GameManager>();
    }

    public void ColumnSave()
    {
        PlayerPrefs.SetInt("ColumnSave", gm.columnSize);
        PlayerPrefs.Save();
    }
    //done
    public void EnemySave()
    {
        PlayerPrefs.SetInt("EnemySave", gm.numEnemyTanks);
        PlayerPrefs.Save();
    }
    //done
    public void MusicSave()
    {
        PlayerPrefs.SetFloat("MusicSave", gm.volumeSlider.value);
        PlayerPrefs.Save();
    }
    //done
    public void SFXSave()
    {
        PlayerPrefs.SetFloat("SFXSave", gm.sfxSlider.value);
        PlayerPrefs.Save();
    }
    //done
    public void HS1Save()
    {
        PlayerPrefs.SetFloat("HS1Save", gm.sd.highScores[0]);
        PlayerPrefs.Save();
    }
    //done
    public void HS2Save()
    {
        PlayerPrefs.SetFloat("HS2Save", gm.sd.highScores[1]);
        PlayerPrefs.Save();
    }
    //done
    public void HS3Save()
    {
        PlayerPrefs.SetFloat("HS3Save", gm.sd.highScores[2]);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        gm.columnSize = PlayerPrefs.GetInt("ColumnSave");
        gm.numEnemyTanks = PlayerPrefs.GetInt("EnemySave");
        gm.volumeSlider.value = PlayerPrefs.GetFloat("MusicSave");
        gm.sfxSlider.value = PlayerPrefs.GetFloat("SFXSave");
        gm.sd.highScores[0] = PlayerPrefs.GetFloat("HS1Save");
        gm.sd.highScores[1] = PlayerPrefs.GetFloat("HS2Save");
        gm.sd.highScores[2] = PlayerPrefs.GetFloat("HS3Save");
    }
}