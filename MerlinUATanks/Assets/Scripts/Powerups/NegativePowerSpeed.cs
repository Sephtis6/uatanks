﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NegativePowerSpeed : Powerup
{
    //overides the void OnAddPowerup in the powerup parent class
    public override void OnAddPowerup(TankData data)
    {
        // remove from the speed by the set amount
        data.moveSpeed -= bonusAmount;
        //inherites from the parent void
        base.OnAddPowerup(data);
    }
    //overides the void OnRemovePowerup in the powerup parent class
    public override void OnRemovePowerup(TankData data)
    {
        //does nothing if it is perment
        if (isPerm)
            return;

        //inherits from the parent void
        base.OnRemovePowerup(data);
        // adds the amount taken from the effected tank
        data.moveSpeed += bonusAmount;
    }
}
