﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour {

    //allows the bullet and shootpoint to be chosen in the editor
    public GameObject bulletPrefab;
    public Transform shootPoint;

    //uses the 2 floats to keep track of how long till another shot can be sent to be decided in the editor
    public float timeToShoot;
    private float shootTime;

    public AudioSource sfxAudio;
    public AudioClip shoot;
    [HideInInspector] public GameObject gm;
    [HideInInspector] public GameManager gameMan;

    // Use this for initialization
    void Start () {
        gm = GameObject.Find("GameManager");
        gameMan = gm.GetComponent<GameManager>();
        sfxAudio = gameMan.sfxAudio;
        shoot = gameMan.audioClips[1];
        shootTime = timeToShoot;

    }
	
	// Update is called once per frame
	void Update () {
        //starts the countdown of how many seconds left till it can shoot
        shootTime -= Time.deltaTime;

        //is the timer is at 0 it will shoot a bullet and then reset the timer till the can shoot again resulting in auto firing
        if (shootTime <= 0)
        {
            sfxAudio.PlayOneShot(shoot, sfxAudio.volume);
            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
            shootTime = timeToShoot;
        }

    }
}
