﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour
{
    //uses the following private variables
    private CharacterController cc;
    [HideInInspector] public Transform tf;
    private TankData data;

    // Use this for initialization
    void Start()
    {
        //sets the variables to the correct components
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //if forward move is called it uses the following function
    public void Move(Vector3 moveVector)
    {
        // Move in the direction of moveVector & speed from tank data
        cc.SimpleMove(moveVector.normalized * data.moveSpeed);
    }

    //if reverse move is called it uses the following function
    public void reverseMove(Vector3 moveVector)
    {
        // Move in the direction of moveVector & speed from tank data
        cc.SimpleMove(-moveVector.normalized * data.moveSpeed);
    }

    //if turn is called it uses the following function
    public void Turn(float direction)
    {
        // Turn in the direction at turn speed
        tf.Rotate(0, Mathf.Sign(direction) * data.turnSpeed * Time.deltaTime, 0);
    }

    public void turnTowards(Vector3 targetDirection)
    {
        //find the rotation that looks down the vector "targetdirection"
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        //rotate "less then turnSpeed" degress towards targetRotation
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed*Time.deltaTime);
    }

}
