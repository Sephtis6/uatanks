﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//mine
public class TankShooter : MonoBehaviour
{
    //allows the bullet and shootpoint to be chosen in the editor
    public GameObject bulletPrefab;
    public Transform shootPoint;
    [HideInInspector] public TankData data;

    //public KeyCode shootKey = KeyCode.Space;
    //uses the 2 floats to keep track of how long till another shot can be sent to be decided in the editor
    public float timeToReload;
    private float timerKeep;

    public AudioSource sfxAudio;
    public AudioClip shoot;
    [HideInInspector] public GameObject gm;
    [HideInInspector] public GameManager gameMan;
    public KeyCode shootKey = KeyCode.Space;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.Find("GameManager");
        gameMan = gm.GetComponent<GameManager>();
        sfxAudio = gameMan.sfxAudio;
        shoot = gameMan.audioClips[1];
        data = GetComponent<TankData>();
        //has the public float and private float set to the same so that they can be reset ot original after the time has run out.
        timerKeep = timeToReload;
    }

    // Update is called once per frame
    void Update()
    {
        //starts a count down from the number that is set
        timerKeep -= Time.deltaTime;
        //spawns a bullet if space is pressed
        if (Input.GetKeyDown (shootKey))
        {
            // bullet will only spawn if the time is at zero
            if (timerKeep <= 0)
            {
                //spawns bullet at chosen shootpoint
                Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
                //resets the time to the chosen time from the editor so that it takes that long till it "reloads"
                //allowing a number of shots per second or minute to be chosen
                timerKeep = timeToReload;
                sfxAudio.PlayOneShot(shoot, sfxAudio.volume);
            }
        }
    }
}

