﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    //places the max health and current health to the same number
    public float maxHealth = 500;
    public float currentHealth = 100;
    public Text lifeText;

    private MapGenerator mg;
    private TankData data;
    private int numberOfPlayers;

    private GameObject playerOne;
    private GameObject playerTwo;

    public List<GameObject> scorePrefabs;

    public GameObject objectToSpawn;


    public AudioSource sfxAudio;
    public AudioClip tankDestroy;
    [HideInInspector] public GameObject gm;
    [HideInInspector] public GameManager gameMan;
    [HideInInspector] public float firstPlayerHealth;
    [HideInInspector] public float secondPlayerHealth;
 
    public void Start()
    {
        gm = GameObject.Find("GameManager");
        gameMan = gm.GetComponent<GameManager>();
        sfxAudio = gameMan.sfxAudio;
        tankDestroy = gameMan.audioClips[3];
        mg = GetComponent<MapGenerator>();
        data = GetComponent<TankData>();
        SetLifeText();
        playerOne = GameObject.Find("PlayerTank One");
        playerTwo = GameObject.Find("PlayerTank Two");
        firstPlayerHealth = gameMan.pStats.firstPlayerHealth;
        secondPlayerHealth = gameMan.pStats.secondPlayerHealth;

        GameObject scorePickUp = Instantiate(scorePrefabs[0]) as GameObject;
        scorePickUp.name = "Score PickUp";
        objectToSpawn = scorePickUp;
    }

    //if the takedamage is initiated from a hit
    public void TakeDamage(int amount)
    {
        //the current health loses based of the ammount the bullet does
        currentHealth -= amount;
        SetLifeText();
        //mg.firstPlayerHealth = playerOne.GetComponent<TankData>().health;
        //mg.secondPlayerHealth = playerTwo.GetComponent<TankData>().health;
        //if the health hits zero it will keep the current health at zero and display that the object is dead and then destory the object it is attached to
        if (currentHealth <= 0)
        {
            sfxAudio.PlayOneShot(tankDestroy, sfxAudio.volume);
            currentHealth = 0;
            Debug.Log("Dead!");
            if (firstPlayerHealth <= 0)
            {
                gameMan.numberOfPlayers = gameMan.numberOfPlayers - 1;
            }
            if (secondPlayerHealth <= 0)
            {
                gameMan.numberOfPlayers = gameMan.numberOfPlayers - 1;
            }
            Destroy(gameObject);
        }
    }

    //set up health text void
    void SetLifeText()
    {
        lifeText.text = "Lives: " + currentHealth.ToString();
    }

    public void makeScore()
    {
        //takes the player tank prefab and generates one named player tank
        GameObject scorePickUp = Instantiate(scorePrefabs[0]) as GameObject;
        scorePickUp.name = "Score PickUp";
        //makes it a child of this tansform
        scorePickUp.transform.parent = this.transform;
        // move it into a random position on the map
        //float XPosition = Random.Range(0.0f, numCols * tileXWidth);
        //float ZPosition = Random.Range(0.0f, numRows * tileZWidth);
        //scorePickUp.transform.localPosition = new Vector3(XPosition, 1.0f, ZPosition);
    }
}
