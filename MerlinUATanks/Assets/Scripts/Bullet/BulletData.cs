﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour {

    //all the items that decide the bullets lifespan and such
    public float moveSpeed = 1.0f;
    public float damageDone = 0;
    public float lifespan = 3.0f;
    public TankData shooter = null;


    private void Start()
    {
        //destroys the object when lifespan is done
        Destroy(gameObject, lifespan);
    }

}
