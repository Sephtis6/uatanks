﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    //data used
    public TankData pawn;
    public List<Transform> waypoints;
    public int currentWaypoint;
    public float waypointCutoff;
    public bool isPatrolling;
    public bool isAdvancingWaypoints = true;
    public float avoidanceDistance;

    public enum PatrolType { Stop, Loop, PingPong, End };
    public PatrolType patrolType;

    public enum AvoidState { Normal, TurnToAvoid, MoveToAvoid };
    public AvoidState avoidState = AvoidState.Normal;
    public float avoidDuration = 1.0f;
    private float avoidStateTime;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
    }

    protected void Idle()
    {
        // DO NOTHING!
    }

    //start chasing player
    protected void Chase()
    {
        MoveTowards(GameManager.instance.player.pawn.mover.tf.position);
    }

    //run from player
    protected void Flee()
    {
        // Get the vector to player
        Vector3 vectorToPlayer = GameManager.instance.player.pawn.mover.tf.position - pawn.mover.tf.position;
        // Flip it (so it now points AWAY from player)
        vectorToPlayer = -1 * vectorToPlayer;
        // Normalize and multiply by speed so it is a short distance away from player
        vectorToPlayer.Normalize();
        // Move towards that new point
        MoveTowards(vectorToPlayer);
    }

    //chase
    protected void MoveTowards(Vector3 target)
    {
        if (avoidState == AvoidState.Normal)
        {
            if (CanMoveForward())
            {
                // Make a new target
                Vector3 targetPosition = new Vector3(target.x,
                                                  pawn.mover.tf.position.y,
                                                  target.z);

                // Find direction to "current" waypoint
                Vector3 dirToWaypoint = targetPosition - pawn.mover.tf.position;

                // Turn towards the "current" waypoint
                pawn.mover.turnTowards(dirToWaypoint);

                // move forward
                pawn.mover.Move(pawn.mover.tf.forward);
            }
            else
            {
                avoidState = AvoidState.TurnToAvoid;
            }
        }
        else if (avoidState == AvoidState.TurnToAvoid)
        {
            pawn.mover.Turn(1);

            if (CanMoveForward())
            {
                avoidState = AvoidState.MoveToAvoid;
                avoidStateTime = Time.time;
            }
        }
        else if (avoidState == AvoidState.MoveToAvoid)
        {
            //If I can Move Forward, do so
            if (CanMoveForward())
            {
                pawn.mover.Move(pawn.mover.tf.forward);
            }
            else
            {
                // If I can't move forward 
                // Go back to my turn to avoid state
                avoidState = AvoidState.TurnToAvoid;
            }

            // If I have moved forward for X seconds
            if (Time.time >= avoidStateTime + avoidDuration)
            {
                // Go back to the normal state
                avoidState = AvoidState.Normal;
            }
        }
    }

    protected bool CanMoveForward()
    {

        Debug.DrawLine(pawn.mover.tf.position, pawn.mover.tf.position + (pawn.mover.tf.forward * avoidanceDistance), Color.red);

        // If I can't move forward (I would hit something)
        if (Physics.Raycast(pawn.mover.tf.position, pawn.mover.tf.forward, avoidanceDistance))
        {
            return false;
        }

        // If I can move forward
        return true;
    }

    protected void Patrol()
    {
        // Make sure my waypoint is within range!!!
        currentWaypoint = Mathf.Clamp(currentWaypoint, 0, waypoints.Count - 1);

        // Move towards waypoint
        MoveTowards(waypoints[currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(pawn.mover.tf.position, waypoints[currentWaypoint].position) <= waypointCutoff)
        {
            // Advance to next waypoint
            if (patrolType == PatrolType.PingPong)
            {
                // Ping pong is the only method that doesn't always move forward
                // Move to the "next" waypoint -- but "next" depends on what direction I'm moving.
                if (isAdvancingWaypoints)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint--;
                }
            }
            else
            {
                currentWaypoint++;
            }

            // Deal with being at the LAST waypoint
            if (currentWaypoint >= waypoints.Count)
            {
                if (patrolType == PatrolType.Loop)
                {
                    // Loop
                    currentWaypoint = 0;
                }
                else if (patrolType == PatrolType.Stop)
                {
                    // Stop
                    isPatrolling = false;
                }
                else if (patrolType == PatrolType.End)
                {
                    // Do Nothing
                }
                else if (patrolType == PatrolType.PingPong)
                {
                    // Reverse directions
                    isAdvancingWaypoints = false;
                    currentWaypoint = currentWaypoint - 2;
                }
            }
            else if (currentWaypoint < 0)
            {
                // Reverse directions (to go forward again)
                isAdvancingWaypoints = true;
                currentWaypoint = 1;
            }
        }
    }
}
