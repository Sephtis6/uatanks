﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryAIController : AIController {

    //data used
    public enum AIStates {Idle, Chase, Flee, Dead};
    public AIStates currentState;
    private float timeInCurrentState = 0;
    public float chaseDistance = 10;
    public float fleeHealthPercent = 10;
    public float fleeTime = 10;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	public override void Update () {
        // Update timer
        timeInCurrentState += Time.deltaTime;

        // Switch based on current state
        switch (currentState) {
            // If I'm in a certain state
            case AIStates.Idle:
                // Perform the actions for that state
                Idle();
                // Check the transitions for that state
                if (Vector3.Distance(pawn.mover.tf.position, GameManager.instance.player.pawn.mover.tf.position) <= chaseDistance)
                {
                    ChangeState(AIStates.Chase);
                }
                if ( (pawn.health.currentHealth/pawn.health.maxHealth) < fleeHealthPercent)
                {
                    ChangeState(AIStates.Flee);
                }
                break;
            case AIStates.Chase:
                Chase();
                if ((pawn.health.currentHealth / pawn.health.maxHealth) < fleeHealthPercent)
                {
                    ChangeState(AIStates.Flee);
                }
                break;
            case AIStates.Flee:
                Flee();
                if (timeInCurrentState > fleeTime)
                {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }

    public void ChangeState ( AIStates newState )
    {
        // Set my state
        currentState = newState;
        // Reset my timer
        timeInCurrentState = 0;
    }
}
