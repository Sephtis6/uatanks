﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    //allows the tankdata to be used through pawn
    public TankData pawn;

    //sets the key codes for forward, back, turn right and turn left
    //to wasd and allows them to be changed in the editor
    public KeyCode forwardKey = KeyCode.W;
    public KeyCode turnRightKey = KeyCode.D;
    public KeyCode backwardKey = KeyCode.S;
    public KeyCode turnLeftKey = KeyCode.A;
    public KeyCode shootKey = KeyCode.Space;

    // Use this for initialization
    void Start()
    {
        //sets the game manager instance of player to this script
        GameManager.instance.player = this;
    }

    // Update is called once per frame
    void Update()
    {
        //if forward key is pressed will move forward the 
        //amount of meters per second from the forwardMove from the tank mover
        if (Input.GetKey(forwardKey))
        {
            pawn.mover.Move(pawn.mover.tf.forward);
        }
        //if turn right key is pressed it will turn once the amount of degrees stated in the turn variables in tank mover right
        if (Input.GetKey(turnRightKey))
        {
            pawn.mover.Turn(1);
        }
        //if backward key is pressed will move forward the 
        //amount of meters per second from the reverseMove from the tank mover
        //they are set in the negatives so it will move backwards instead of forward
        if (Input.GetKey(backwardKey))
        {
            pawn.mover.Move(-pawn.mover.tf.forward);
        }
        //if turn left key is pressed it will turn once the amount of degrees stated in the turn variables in tank mover left
        if (Input.GetKey(turnLeftKey))
        {
            pawn.mover.Turn(-1);
        }

        //pawn.SendMessage("SayHello", SendMessageOptions.DontRequireReceiver);
    }
}
