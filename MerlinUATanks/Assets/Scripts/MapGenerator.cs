﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    //data used
    public bool randomMap;
    public bool mapOfTheDay;
    public bool presetSeed;
    public bool multiPlayer;

    public List<Room> rooms;
    public float tileXWidth;
    public float tileZWidth;

    public int numCols;
    public int numRows;

    public int mapSeed;

    public List<GameObject> tilePrefabs;
    public List<GameObject> enemyTankPrefabs;
    public List<GameObject> playerOneTankPrefab;
    public List<GameObject> playerTwoTankPrefab;
    public int numEnemyTanks;

    private int numberOfPlayers;

    [HideInInspector] public GameObject gm;
    [HideInInspector] public GameManager gameMan;

    public Text p1lifeText;
    public Text p2lifeText;
    public Text p1ScoreText;
    public Text p2scoreText;

    [HideInInspector] public TankData p1data;
    [HideInInspector] public TankData p2data;

    [HideInInspector] public float firstPlayerHealth;
    [HideInInspector] public float firstPlayerScore;
    [HideInInspector] public float secondPlayerHealth;
    [HideInInspector] public float secondPlayerScore;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.Find("GameManager");
        gameMan = gm.GetComponent<GameManager>();
        randomMap = gameMan.randomMap;
        mapOfTheDay = gameMan.mapOfTheDay;
        multiPlayer = gameMan.multiPlayer;
        numCols = gameMan.columnSize;
        numRows = numCols;
        numEnemyTanks = gameMan.numEnemyTanks;
        numberOfPlayers = gameMan.numberOfPlayers;
        firstPlayerHealth = gameMan.pStats.firstPlayerHealth;
        firstPlayerScore = gameMan.pStats.firstPlayerScore;
        secondPlayerHealth = gameMan.pStats.secondPlayerHealth;
        secondPlayerScore = gameMan.pStats.secondPlayerScore;

        //if the random map bool is true run the build map function
        if (randomMap == true)
        {
            //calls the following void
            BuildMap();
        }
        //if the preset seed map bool is true run the build preset seed map function
        if (presetSeed == true)
        {
            //calls the following void
            //setting our seed
            Random.seed = mapSeed;
            BuildMap();
        }
        //if the map of the day bool is true run the map of the day function
        if (mapOfTheDay == true)
        {
            //calls the following void
            // Set our seed
            Random.seed = System.DateTime.Now.Day;
            BuildMap();
        }
    }

    // Update is called once per frame
    void Update()
    {
        randomMap = gameMan.randomMap;
        mapOfTheDay = gameMan.mapOfTheDay;
        multiPlayer = gameMan.multiPlayer;
        numCols = gameMan.columnSize;
        firstPlayerHealth = gameMan.pStats.firstPlayerHealth;
        firstPlayerScore = gameMan.pStats.firstPlayerScore;
        secondPlayerHealth = gameMan.pStats.secondPlayerHealth;
        secondPlayerScore = gameMan.pStats.secondPlayerScore;
        setPlayerOneLifeText();
        setPlayerTwoLifeText();
        setPlayerOneScoreText();
        setPlayerTwoScoreText();
    }

    //this is the build random map void
    public void BuildMap()
    {
        //creates a map based on how many rows and columns are requested in the developer
        //starts off with one row being made and then looping through the other rows untill all are made
        for (int currentRow = 0; currentRow < numRows; currentRow++)
        {
            //starts off with one column bein made and then loops around till all the colums are done then loops through the rows with new collumns being made
            for (int currentCol = 0; currentCol < numCols; currentCol++)
            {
                // pulls a prefab from the list
                int rand = Random.Range(0, tilePrefabs.Count);
                GameObject newTile = Instantiate(tilePrefabs[rand]) as GameObject;
                // then names it based on what row nad column it is put in
                newTile.name = "Tile (" + currentCol + "," + currentRow + ")";
                // Make this tile a child of THIS object
                newTile.transform.parent = this.transform;

                // moves it into the position needed in order to make it nice and neat with no gaps
                float XPosition = currentCol * tileXWidth;
                float ZPosition = currentRow * tileZWidth;
                newTile.transform.localPosition = new Vector3(XPosition, 0.0f, ZPosition);

            }
        }
        //the instantiats the player and enemy tanks into the field
        if (multiPlayer == false)
        {
            makePlayerTank();
            gameMan.numberOfPlayers = 1;
        }
        if (multiPlayer == true)
        {
            makeFirstPlayerTank();
            makeSecondPlayerTank();
            gameMan.numberOfPlayers = 2;
        }
        makeEnemyTank();
    }


    //puts the frist player tank onto the map
    public void makePlayerTank()
    {
        //takes the player tank prefab and generates one named player tank
        GameObject newFirstPlayerTank = Instantiate(playerOneTankPrefab[0]) as GameObject;
        newFirstPlayerTank.name = "PlayerTank One";
        //makes it a child of this tansform
        newFirstPlayerTank.transform.parent = this.transform;
        // move it into a random position on the map
        float XPosition = Random.Range(0.0f, numCols * tileXWidth);
        float ZPosition = Random.Range(0.0f, numRows * tileZWidth);
        newFirstPlayerTank.transform.localPosition = new Vector3(XPosition, 1.0f, ZPosition);
    }

    public void makeFirstPlayerTank()
    {
        //takes the player tank prefab and generates one named player tank
        GameObject newFirstPlayerTank = Instantiate(playerOneTankPrefab[1]) as GameObject;
        newFirstPlayerTank.name = "PlayerTank One";
        //makes it a child of this tansform
        newFirstPlayerTank.transform.parent = this.transform;
        // move it into a random position on the map
        float XPosition = Random.Range(0.0f, numCols * tileXWidth);
        float ZPosition = Random.Range(0.0f, numRows * tileZWidth);
        newFirstPlayerTank.transform.localPosition = new Vector3(XPosition, 1.0f, ZPosition);
    }

    //puts the second player tank onto the map
    public void makeSecondPlayerTank()
    {
        //takes the player tank prefab and generates one named player tank
        GameObject newSecondPlayerTank = Instantiate(playerTwoTankPrefab[0]) as GameObject;
        newSecondPlayerTank.name = "PlayerTank Two";
        //makes it a child of this tansform
        newSecondPlayerTank.transform.parent = this.transform;
        // move it into a random position on the map
        float XPosition = Random.Range(0.0f, numCols * tileXWidth);
        float ZPosition = Random.Range(0.0f, numRows * tileZWidth);
        newSecondPlayerTank.transform.localPosition = new Vector3(XPosition, 1.0f, ZPosition);
    }

    //creates a number of tanks based on various inpouts
    public void makeEnemyTank()
    {
        //loops till the number of enemy tanks the developer wants are made
        for (int enemyTank = 0; enemyTank < numEnemyTanks; enemyTank++)
        {
            //takes one from a list that can be added to as different tank personalities are made
            int randTank = Random.Range(0, enemyTankPrefabs.Count);
            GameObject newEnemyTank = Instantiate(enemyTankPrefabs[randTank]) as GameObject;
            //gives the tanks a name based on what number in the "prduction" line they are
            newEnemyTank.name = "Enemy Tank (" + enemyTank + ")";
            //makes it a child of this tansform
            newEnemyTank.transform.parent = this.transform;
            // move it into a random position on the map
            float XPosition = Random.Range(0.0f, numCols * tileXWidth);
            float ZPosition = Random.Range(0.0f, numRows * tileZWidth);
            newEnemyTank.transform.localPosition = new Vector3(XPosition, 1.0f, ZPosition);
        }
    }

    void setPlayerOneLifeText()
    {
    p1ScoreText.text = "Player One Health: " + firstPlayerHealth.ToString();
    }

    void setPlayerTwoLifeText()
    {
    p1ScoreText.text = "Player Two Health: " + secondPlayerHealth.ToString();
    }

    void setPlayerOneScoreText()
    {
    p1ScoreText.text = "Player One Score: " + firstPlayerScore.ToString();
    }


    void setPlayerTwoScoreText()
    {
    p1lifeText.text = "Player Two Score: " + secondPlayerScore.ToString();
    }
}
